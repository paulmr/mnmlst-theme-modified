<div id="footer">
  <p>
    Dankon al <a href="http://mnmlist.com/theme">mnmlist.com</a> pro la bonega WordPress-ha&#365;to.
  </p>
  <p>
    <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />Enhavon de ĉi tiu retejo verkis <a xmlns:cc="http://creativecommons.org/ns#" href="http://stelo.org.uk" property="cc:attributionName" rel="cc:attributionURL">Paul Roberts</a>. Ĝi estas eldonata laŭ permesilo <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US">Creative Commons Attribution 3.0 Unported License</a>.
  </p>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
