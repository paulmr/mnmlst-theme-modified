DELIVS=404.php _README.txt allposts.php footer.php header.php index.php page.php screenshot.png single.php style.css

DISTFILE=mnmlist.zip

RSYNC_EXCLUDES=rsync-excludes.txt

RSYNC_OPTS+=-acvz --chmod=Da+rx,Fa+r

dist:	$(DELIVS)
	(zip -u $(DISTFILE) $(DELIVS); RES=$$?; \
	if [[ $${RES} -eq 12 ]]; then exit 0; else exit $${RES}; fi;)

rsync:  $(DELIVS)
	rsync --exclude-from=$(RSYNC_EXCLUDES) $(RSYNC_OPTS) ./ $(BLOG_RSYNC_USER)@$(BLOG_RSYNC_HOST):$(BLOG_RSYNC_PATH)/

clean:
	rm -f $(DISTFILE)
